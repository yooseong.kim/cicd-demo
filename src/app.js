const express = require('express');
const app = express();

// returns a simple respnse 
app.get('/', (req, res) => {
  console.log(`received request: ${req.method} ${req.url}`)
  let retvalue = `<h1>Hello... <br><br>
                  GitLab, Jenkins, GKE 연동 테스트입니다. <br><br> 
                  - VERSION : <span style="color:red">YYYY</span></h1>`;

  res.status(200).send(retvalue);
});

// starts an http server on the $PORT environment variable
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

module.exports = app
